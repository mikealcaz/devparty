import { builder } from '@graphql/builder'
import { db } from '@utils/prisma'

import { reservedSlugs } from '../Common/queries/reservedSlugs'
import { createProduct } from './mutations/createProduct'
import { toggleSubscribe } from './mutations/toggleSubscribe'
import { getProducts } from './queries/getProducts'
import { hasSubscribed } from './queries/hasSubscribed'

builder.prismaObject('Product', {
  findUnique: (post) => ({ id: post.id }),
  fields: (t) => ({
    id: t.exposeID('id'),
    name: t.exposeString('name'),
    slug: t.exposeString('slug'),
    description: t.exposeString('description', { nullable: true }),
    avatar: t.exposeString('avatar', { nullable: true }),
    website: t.exposeString('website', { nullable: true }),
    producthunt: t.exposeString('producthunt', { nullable: true }),
    discord: t.exposeString('discord', { nullable: true }),
    github: t.exposeString('github', { nullable: true }),
    twitter: t.exposeString('twitter', { nullable: true }),
    hasSubscribed: t.field({
      type: 'Boolean',
      resolve: async (parent, args, { session }) => {
        if (!session) return false
        return await hasSubscribed(session?.userId as string, parent.id)
      }
    }),

    // Timestamps
    createdAt: t.expose('createdAt', { type: 'DateTime' }),
    updatedAt: t.expose('updatedAt', { type: 'DateTime' }),

    // Relations
    owner: t.relation('owner'),
    posts: t.relatedConnection('posts', {
      cursor: 'id',
      totalCount: true,
      query: () => ({
        where: { hidden: false },
        orderBy: { createdAt: 'desc' }
      })
    })
  })
})

builder.queryField('products', (t) =>
  t.prismaConnection({
    type: 'Product',
    cursor: 'id',
    defaultSize: 20,
    maxSize: 100,
    resolve: async (query) => {
      return await getProducts(query)
    }
  })
)

builder.queryField('product', (t) =>
  t.prismaField({
    type: 'Product',
    args: { slug: t.arg.string() },
    resolve: async (query, parent, { slug }) => {
      return await db.product.findUnique({
        ...query,
        where: { slug },
        rejectOnNotFound: true
      })
    }
  })
)

const CreateProductInput = builder.inputType('CreateProductInput', {
  fields: (t) => ({
    name: t.string({ validate: { minLength: 2, maxLength: 50 } }),
    slug: t.string({ validate: { minLength: 2, maxLength: 50 } }),
    website: t.string({
      validate: { minLength: 2, maxLength: 100, url: true }
    }),
    description: t.string({ required: false, validate: { maxLength: 190 } })
  })
})

builder.mutationField('createProduct', (t) =>
  t.prismaField({
    type: 'Product',
    args: { input: t.arg({ type: CreateProductInput }) },
    resolve: async (query, parent, { input }, { session }) => {
      return await createProduct(query, input, session)
    }
  })
)

const EditProductInput = builder.inputType('EditProductInput', {
  fields: (t) => ({
    id: t.id(),
    slug: t.string({
      required: true,
      validate: { minLength: 2, maxLength: 50 }
    }),
    name: t.string({
      required: true,
      validate: { minLength: 2, maxLength: 50 }
    }),
    description: t.string({ required: false, validate: { maxLength: 255 } }),
    avatar: t.string({ required: false })
  })
})

// TODO: Split to function
builder.mutationField('editProduct', (t) =>
  t.prismaField({
    type: 'Product',
    args: { input: t.arg({ type: EditProductInput }) },
    authScopes: { user: true },
    nullable: true,
    resolve: async (query, parent, { input }) => {
      if (reservedSlugs.includes(input.slug)) {
        throw new Error(`Product slug "${input.slug}" is reserved by Devparty.`)
      }

      try {
        return await db.product.update({
          ...query,
          where: { id: input?.id },
          data: {
            slug: input.slug,
            name: input.name,
            description: input.description,
            avatar: input.avatar
          }
        })
      } catch (error: any) {
        if (error.code === 'P2002') {
          throw new Error('Product slug is already taken!')
        }

        throw new Error(
          process.env.NODE_ENV === 'production'
            ? 'Something went wrong!'
            : error
        )
      }
    }
  })
)

const ToggleProductSubscribeInput = builder.inputType(
  'ToggleProductSubscribeInput',
  {
    fields: (t) => ({
      id: t.id()
    })
  }
)

builder.mutationField('toggleProductSubscribe', (t) =>
  t.prismaField({
    type: 'Product',
    args: { input: t.arg({ type: ToggleProductSubscribeInput }) },
    nullable: true,
    resolve: async (query, parent, { input }, { session }) => {
      return await toggleSubscribe(session?.userId as string, input.id)
    }
  })
)

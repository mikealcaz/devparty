import { gql, useMutation } from '@apollo/client'
import { GridItemEight, GridItemFour, GridLayout } from '@components/GridLayout'
import SettingsHelper from '@components/shared/SettingsHelper'
import { Button } from '@components/ui/Button'
import { Card, CardBody } from '@components/ui/Card'
import { ErrorMessage } from '@components/ui/ErrorMessage'
import { Form, useZodForm } from '@components/ui/Form'
import { Input } from '@components/ui/Input'
import { Spinner } from '@components/ui/Spinner'
import { SuccessMessage } from '@components/ui/SuccessMessage'
import { TextArea } from '@components/ui/TextArea'
import { uploadToIPFS } from '@components/utils/uploadToIPFS'
import { CheckCircleIcon } from '@heroicons/react/outline'
import Link from 'next/link'
import React, { useEffect, useState } from 'react'
import toast from 'react-hot-toast'
import { Product } from 'src/__generated__/schema.generated'
import { object, string } from 'zod'

import {
  ProductSettingsMutation,
  ProductSettingsMutationVariables
} from './__generated__/ProductSettings.generated'

const editProductSchema = object({
  slug: string()
    .min(2, { message: '📦 Slug should be atleast 2 characters' })
    .max(50, { message: '📦 Slug should not exceed 100 characters' })
    .regex(/^[a-z0-9_\.]+$/, { message: '📦 Invalid slug' }),
  name: string()
    .min(2, { message: '🍀 Name should be atleast 2 characters' })
    .max(50, { message: '🍀 Name should not exceed 100 characters' }),
  description: string()
    .max(190, { message: '📦 Description should not exceed 190 characters' })
    .nullable()
})

interface Props {
  product: Product
}

const SUCCESS_MESSAGE = 'Product successfully updated!'

const ProductSettings: React.FC<Props> = ({ product }) => {
  const [avatar, setAvatar] = useState<string>()
  const [editProduct, editProductResult] = useMutation<
    ProductSettingsMutation,
    ProductSettingsMutationVariables
  >(
    gql`
      mutation ProductSettingsMutation($input: EditProductInput!) {
        editProduct(input: $input) {
          id
          slug
          name
          description
        }
      }
    `,
    {
      onCompleted() {
        toast.success(SUCCESS_MESSAGE)
      }
    }
  )

  useEffect(() => {
    if (product?.avatar) setAvatar(product?.avatar)
  }, [product])

  const handleUpload = async (evt: any) => {
    evt.preventDefault()
    // setLoading({ type, status: true })

    try {
      const attachment = await uploadToIPFS(evt.target.files)
      setAvatar(attachment.url)
    } finally {
      // setLoading({ type, status: false })
    }
  }

  const form = useZodForm({
    schema: editProductSchema,
    defaultValues: {
      slug: product?.slug,
      name: product?.name,
      description: product?.description
    }
  })

  return (
    <GridLayout>
      <GridItemFour>
        <SettingsHelper
          heading="Product settings"
          description={'Update your product information and identities.'}
        />
      </GridItemFour>
      <GridItemEight>
        <Card>
          <CardBody>
            <Form
              form={form}
              className="space-y-4"
              onSubmit={({ slug, name, description }) =>
                editProduct({
                  variables: {
                    input: {
                      id: product?.id,
                      slug,
                      name,
                      description,
                      avatar: avatar as string
                    }
                  }
                })
              }
            >
              <ErrorMessage
                title="Error updating profile"
                error={editProductResult.error}
              />
              {editProductResult.data && (
                <SuccessMessage>{SUCCESS_MESSAGE}</SuccessMessage>
              )}
              <Input label="ID" type="text" value={product?.id} disabled />
              <Input
                label="Slug"
                type="text"
                placeholder="minecraft"
                prefix="https://devparty.io/products/"
                {...form.register('slug')}
              />
              <Input
                label="Name"
                type="text"
                placeholder="John Doe"
                {...form.register('name')}
              />
              <TextArea
                label="Description"
                placeholder="Tell us about the product!"
                {...form.register('description')}
              />
              <div className="space-y-1.5">
                <label>Avatar</label>
                <div className="flex items-center gap-3">
                  <img
                    className="rounded-full h-24 w-24"
                    src={avatar}
                    alt={avatar}
                  />
                  <input type="file" onChange={(evt) => handleUpload(evt)} />
                </div>
              </div>
              <div className="flex items-center justify-between pt-3">
                <Link href="/settings/password">Change password?</Link>
                <Button
                  type="submit"
                  icon={
                    form.formState.isSubmitting ? (
                      <Spinner size="xs" />
                    ) : (
                      <CheckCircleIcon className="h-4 w-4" />
                    )
                  }
                >
                  Save
                </Button>
              </div>
            </Form>
          </CardBody>
        </Card>
      </GridItemEight>
    </GridLayout>
  )
}

export default ProductSettings

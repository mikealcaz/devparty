export const userData = [
  {
    email: 'me@yogi.codes',
    username: 'yoginth',
    name: 'Yoginth',
    isStaff: true,
    avatar: 'https://i.ibb.co/Kw0Jbzf/avatar.png',
    bio: '요기 • Creator of @Taskord • Mod at @thepracticaldev • BTS Fanboi ⟬⟭ • he/him 🏳️‍🌈🌳'
  },
  {
    email: 'filiptronicek@yogi.codes',
    username: 'filiptronicek',
    name: 'Filip Troníček',
    isStaff: true,
    avatar: 'https://avatars.githubusercontent.com/u/29888641',
    bio: 'A weird 16-year old who loves JavaScript and Python for a better world with OSS'
  },
  {
    email: 'svobodavl@yogi.codes',
    username: 'svobodavl',
    name: 'Vláďa Svoboda',
    isStaff: false,
    avatar: 'https://avatars.githubusercontent.com/u/58887042',
    bio: '“Dark mode everything”'
  },
  {
    email: 'aellopos@yogi.codes',
    username: 'aellopos',
    name: 'Michal Kučera',
    isStaff: false,
    avatar: 'https://avatars.githubusercontent.com/u/39790985',
    bio: 'Czechitas Senior lecturer | Microsoft STC Learning & Development Lead | Microsoft MLSA Country Lead of Czech Republic'
  },
  {
    email: 'kahy@yogi.codes',
    username: 'kahy',
    name: 'Josef Kahoun',
    isStaff: false,
    avatar: 'https://avatars.githubusercontent.com/u/48121432'
  }
]
